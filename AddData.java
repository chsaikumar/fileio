import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This is the ADDdata class to insert data into the file
 * 
 * @author schinthakunta
 *
 */
public class AddData {
	@SuppressWarnings("unchecked")
	public void addData() throws IOException {
		File file = new File("C:\\Users\\schinthakunta\\eclipse-workspace\\Fileio\\src\\newfile.txt");
		FileWriter filewriter = new FileWriter(file);
		
		//creating the object for firstusers
		JSONObject firstuser = new JSONObject();
		firstuser.put("firstName", "Saikumar");
		firstuser.put("age", "23");
		firstuser.put("designation", "IT emplyee");
		firstuser.put("location", "Hyderabad");

		JSONArray listofhobbies = new JSONArray();
		listofhobbies.add("cricket");
		listofhobbies.add("Playing");
		firstuser.put("hobbies", listofhobbies);

		JSONObject userObject = new JSONObject();
		userObject.put("user", firstuser);

		// Second user
		JSONObject seconduser = new JSONObject();
		seconduser.put("firstName", "Ashok");
		seconduser.put("age", "25");
		seconduser.put("designation", "software");
		JSONArray listofhobbies2 = new JSONArray();
		listofhobbies2.add("hockey");
		listofhobbies2.add("games");
		seconduser.put("hobbies", listofhobbies2);

		JSONObject userObject2 = new JSONObject();
		userObject2.put("user", seconduser);
		
		//Third user
		JSONObject thirduser =new JSONObject();
		thirduser.put("Name", "anil");
		thirduser.put("designation", "Software Engineer");
		thirduser.put("interest", "Playing");
		
		JSONObject userobject3=new JSONObject();
		userobject3.put("user", thirduser);

		// Add users to list
		JSONArray userList = new JSONArray();
		userList.add(userObject);
		userList.add(userObject2);
		userList.add(userobject3);

		// Write JSON file
		filewriter.write(userList.toJSONString());
		filewriter.flush();
		filewriter.close();
	}
}
